import 'package:exspeedy/providers/authentication_provider.dart';
import 'package:exspeedy/utils/constants.dart';
import 'package:exspeedy/utils/functions.dart';
import 'package:exspeedy/utils/size_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _firstNameController = TextEditingController();
  TextEditingController _lastNameController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _passwordConfirmationController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        children: [
          AppBar(
            backgroundColor: Colors.transparent,
            iconTheme: IconThemeData(
              color: Colors.grey[800]
            ),
            title: Text(
              'Inscription',
              style: TextStyle(
                color: Colors.grey[800]
              ),
            ),
          ),
          Container(
            width: double.infinity,
            height: 1,
            decoration: BoxDecoration(color: Colors.grey.withOpacity(0.3)),
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: getScreenWidth(20)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(height: SizeConfig.screenHeight * 0.04,),
                    Text(
                      'Veuillez remplir les informations ci-dessous\npour créer votre compte.',
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: SizeConfig.screenHeight * 0.08,),
                    _buildSignUpForm(),
                    SizedBox(height: SizeConfig.screenHeight * 0.1,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                            'Vous avez déjà un compte?'
                        ),
                        FlatButton(
                          onPressed: (){
                            Navigator.of(context).pop();
                          },
                          child: Text(
                            'Connectez vous',
                            style: TextStyle(
                                color: kPrimaryColor
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: SizeConfig.screenHeight * 0.1,),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _buildSignUpForm(){
    return Form(
      key: _formKey,
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: TextFormField(
                  controller: _firstNameController,
                  keyboardType: TextInputType.text,
                  validator: (v){
                    if(v.isEmpty){
                      return 'Entrez votre nom';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    hintText: 'Nom',
                  ),
                ),
              ),
              SizedBox(width: getScreenWidth(15),),
              Expanded(
                child: TextFormField(
                  controller: _lastNameController,
                  keyboardType: TextInputType.text,
                  validator: (v){
                    if(v.isEmpty){
                      return 'Entrez votre Prénom';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    hintText: 'Prénom',
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: getScreenHeight(15),),
          TextFormField(
            controller: _phoneController,
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))
            ],
            validator: (v){
              if(v.isEmpty){
                return 'Entrez votre numéro de téléphone';
              }
              return null;
            },
            decoration: InputDecoration(
              hintText: 'Téléphone',
            ),
          ),
          SizedBox(height: getScreenHeight(15),),
          TextFormField(
            obscureText: true,
            controller: _passwordController,
            validator: (v){
              if(v.isEmpty){
                return "Entrez votre mot de passe";
              }else if(v.length < 6){
                return "Entrez un mot de passe plus long";
              }
              return null;
            },
            decoration: InputDecoration(
                hintText: 'Mot de passe'
            ),
          ),
          SizedBox(height: getScreenHeight(15),),
          TextFormField(
            obscureText: true,
            controller: _passwordConfirmationController,
            validator: (v){
              if(v.isEmpty){
                return "Re-entrez votre mot de passe";
              }else if(v != _passwordController.text){
                return "Veuillez entrer le même mot de passe";
              }
              return null;
            },
            decoration: InputDecoration(
                hintText: 'Confirmez le mot de passe'
            ),
          ),
          SizedBox(height: getScreenHeight(25),),
          RaisedButton(
            onPressed: () {
              hideKeyBoard();
              if(_formKey.currentState.validate()){
                signUp();
              }
              // Navigator.of(context).pushNamed(HomeScreen.routeName);
            },
            child: Container(
              width: double.infinity,
              height: getScreenHeight(60),
              child: Center(
                  child: Text(
                    'Connexion',
                    style: TextStyle(
                        fontSize: getScreenWidth(16),
                        color: Colors.white
                    ),
                  )
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> signUp() async{
    final _authNotifier = context.read(authenticationNotifierProvider);
    await _authNotifier.signUp(_firstNameController.text, _lastNameController.text,
        _phoneController.text, _passwordController.text);
  }
}
