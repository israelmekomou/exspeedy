import 'package:exspeedy/screens/checkout/checkout_screen.dart';
import 'package:exspeedy/screens/home/slider_item.dart';
import 'package:exspeedy/utils/constants.dart';
import 'package:exspeedy/utils/default_button.dart';
import 'package:exspeedy/utils/size_config.dart';
import 'package:flutter/material.dart';

class HomeItem extends StatefulWidget {
  @override
  _HomeItemState createState() => _HomeItemState();
}

class _HomeItemState extends State<HomeItem> {
  int _currentPage = 0;
  List<Map<String, String>> sliderData = [
    {
      "text": "Text 1",
      "image": "assets/images/splash_1.png"
    },
    {
      "text": "Text 2",
      "image": "assets/images/splash_2.png"
    },
    {
      "text": "Text 3",
      "image": "assets/images/splash_3.png"
    },
  ];
  
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey.withOpacity(0.2),
      child: Column(
        children: [
          SizedBox(height: SizeConfig.screenHeight * 0.04,),
          Expanded(
            flex: 3,
            child: PageView.builder(
              itemCount: sliderData.length,
              onPageChanged: (page){
                setState(() {
                  _currentPage = page;
                });
              },
              itemBuilder: (context, index){
                Map<String, String> item = sliderData[index];
                return SliderContent(
                  text: item['text'],
                  image: item['image'],
                );
              },
            ),
          ),
          Expanded(
            flex: 2,
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: getScreenWidth(20)
              ),
              child: Column(
                children: [
                  Spacer(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: List.generate(
                      sliderData.length,
                      (index) => buildDot(index)
                    ),
                  ),
                  Spacer(flex: 1,),
                  DefaultButton(
                    text: 'Expédier un colis',
                    onPress:() => Navigator.of(context).pushNamed(CheckoutScreen.routeName)
                  ),
                  Spacer(flex: 1,),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  AnimatedContainer buildDot(int index){
    return AnimatedContainer(
      duration: KAnimationDuration,
      margin: EdgeInsets.only(right: 5),
      height: 6,
      width: _currentPage == index ? 20 : 6,
      decoration: BoxDecoration(
        color: _currentPage == index ? kPrimaryColor : Color(0xFFD8D8D8),
        borderRadius: BorderRadius.circular(3)
      ),
    );
  }
}
