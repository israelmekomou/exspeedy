import 'package:exspeedy/screens/home/BottomNavBarItem.dart';
import 'package:exspeedy/screens/home/body.dart';
import 'package:exspeedy/screens/home/custom_bottom_navigation_bar.dart';
import 'package:exspeedy/screens/home/my_orders_item.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  static String routeName = '/home';
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _currentPageIndex = 1;
  List<BottomNavBarItem> _navBarItems = [];
  @override
  void initState() {
    _navBarItems.add(BottomNavBarItem(
      title: 'Profils',
      icon: Icons.person_rounded,
    ));
    _navBarItems.add(BottomNavBarItem(
      title: 'Accueil',
      icon: Icons.home_rounded,
    ));
    _navBarItems.add(BottomNavBarItem(
      title: 'Mes expéditions',
      icon: Icons.list_rounded,
    ));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 0,
      ),
      body: Body(_currentPageIndex, _navBarItems),
      bottomNavigationBar: CustomBottomNavigationBar(
          navBarItems: _navBarItems,
          defaultSelectedIndex: 1,
          onIndexChange: (index){
            setState(() {
              _currentPageIndex = index;
            });
          }),
    );
  }
}
