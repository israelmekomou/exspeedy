import 'package:exspeedy/models/order.dart';
import 'package:exspeedy/providers/order_provider.dart';
import 'package:exspeedy/screens/home/my_orders_controller.dart';
import 'package:exspeedy/screens/home/order_history_list_item.dart';
import 'package:exspeedy/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class MyOrdersItem extends StatefulWidget {
  final MyOrdersController controller;

  MyOrdersItem({this.controller});

  @override
  _MyOrdersItemState createState() => _MyOrdersItemState(controller);
}

class _MyOrdersItemState extends State<MyOrdersItem> {
  _MyOrdersItemState(MyOrdersController _controller){
    _controller.refresh = initOrder;
  }

  Future<List<Order>> _orders;

  @override
  void initState() {
    _orders = _fetchOrders();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: Colors.grey.withOpacity(0.2),
      child: _orders == null ? SizedBox() : FutureBuilder<List<Order>>(
        future: _orders,
        builder: (context, snapshot){
          switch (snapshot.connectionState){
            case ConnectionState.none:
              return Text('NONE');
            case ConnectionState.done:
              return ListView.builder(
                  padding: EdgeInsets.symmetric(horizontal: getScreenWidth(10)),
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index){
                    return OrderHistoryListItem(snapshot.data[index], index == snapshot.data.length - 1);
                  }
              );
            case ConnectionState.waiting:
            default:
              return Center(
                child: Container(
                    height: getScreenWidth(40),
                    width: getScreenWidth(40),
                    child: CircularProgressIndicator()
                ),
              );
          }
        },
      ),
    );
  }

  Future<List<Order>> _fetchOrders() {
    final _orderRepository = context.read(orderRepositoryProvider);
    return _orderRepository.getOrders();
  }

  void initOrder() {
    setState(() {
      _orders = _fetchOrders();
    });
  }
}
