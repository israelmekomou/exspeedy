import 'package:exspeedy/providers/user_provider.dart';
import 'package:exspeedy/screens/home/BottomNavBarItem.dart';
import 'package:exspeedy/screens/home/home_item.dart';
import 'package:exspeedy/screens/home/my_orders_controller.dart';
import 'package:exspeedy/screens/home/my_orders_item.dart';
import 'package:exspeedy/screens/home/profile_item.dart';
import 'package:exspeedy/utils/constants.dart';
import 'package:exspeedy/custom_widgets/default_background.dart';
import 'package:flutter/material.dart';

import 'package:flutter_riverpod/flutter_riverpod.dart';

class Body extends StatefulWidget {
  final int currentPageIndex;
  final List<BottomNavBarItem> navBarItems;

  Body(this.currentPageIndex, this.navBarItems);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  final _myOrdersController = MyOrdersController();

  @override
  void initState() {
    final _userStateNotifier = context.read(userStateNotifierProvider);
    _userStateNotifier.getUserProfile();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultBackground(
      title: Stack(
        children: [
          Center(
            child: Text(
              widget.navBarItems[widget.currentPageIndex].title.toUpperCase(),
              style: headingStyle
            ),
          ),
          Visibility(
            visible: widget.currentPageIndex == 2,
            child: Positioned(
              right: 5,
              top: 0,
              bottom: 0,
              child: IconButton(
                icon: Icon(
                  Icons.refresh_rounded,
                  color: Colors.white,
                ),
                onPressed: _myOrdersController.refresh,
              ),
            ),
          )
        ],
      ),
      body: IndexedStack(
        index: widget.currentPageIndex,
        children: [
          ProfileItem(),
          HomeItem(),
          MyOrdersItem(
            controller: _myOrdersController
          )
        ],
      ),
      position: widget.currentPageIndex,
    );
  }
}
