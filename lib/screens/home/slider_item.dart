import 'package:exspeedy/utils/constants.dart';
import 'package:exspeedy/utils/size_config.dart';
import 'package:flutter/material.dart';

class SliderContent extends StatelessWidget {
  final String text, image;
  SliderContent({this.text, this.image});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          kAppName,
          style: TextStyle(
            fontSize: getScreenWidth(30),
            color: kPrimaryColor,
            fontWeight: FontWeight.bold
          ),
        ),
        SizedBox(height: getScreenHeight(5),),
        Text(
          text,
          textAlign: TextAlign.center,
        ),
        Spacer(flex: 2,),
        Image.asset(
          image,
          height: getScreenHeight(200),
          width: getScreenWidth(200),
        )
      ],
    );
  }
}
