import 'package:exspeedy/providers/authentication_provider.dart';
import 'package:exspeedy/screens/sign_in/sign_in_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ProfileItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _authRepository = context.read(authenticationRepositoryProvider);
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: Colors.grey.withOpacity(0.2),
      child: Center(
        child: IconButton(
          icon: Icon(Icons.logout),
          onPressed: (){
            _authRepository.deleteToken();
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (_) => SignInScreen()),
                (route) => false
            );
          },
        ),
      ),
    );
  }
}
