import 'package:flutter/material.dart';

class BottomNavBarItem{
  String title;
  IconData icon;

  BottomNavBarItem({this.title, this.icon});
}