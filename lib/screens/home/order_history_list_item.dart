import 'package:exspeedy/models/order.dart';
import 'package:exspeedy/utils/constants.dart';
import 'package:exspeedy/utils/size_config.dart';
import 'package:flutter/material.dart';

class OrderHistoryListItem extends StatelessWidget {
  final Order _order;
  final bool isLast;

  OrderHistoryListItem(this._order, this.isLast);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 20,),
        Container(
          padding: EdgeInsets.all(getScreenWidth(10)),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
          ),
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: kPrimaryColor.withOpacity(0.5)
                      ),
                      child: Center(
                        child: Text(
                          _order.orderInfo.humanStatus,
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Center(
                      child: Text(
                        _order.orderInfo.shippingDate,
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: getScreenHeight(5),),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Contenu: ',
                    style: TextStyle(
                        fontWeight: FontWeight.bold
                    ),
                  ),
                  Expanded(
                    child: Text(
                      _order.orderInfo.content,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
              SizedBox(height: getScreenHeight(5),),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Destination: ',
                    style: TextStyle(
                        fontWeight: FontWeight.bold
                    ),
                  ),
                  Expanded(
                    child: Text(
                      '${_order.receiver.adresses[0].ville}(${_order.receiver.adresses[0].quartier})',
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
              SizedBox(height: getScreenHeight(5),),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Frais: ',
                    style: TextStyle(
                        fontWeight: FontWeight.bold
                    ),
                  ),
                  Expanded(
                    child: Text(
                      '${_order.orderPayment.totalAmount} FCFA',
                      style: TextStyle(
                          color: kPrimaryColor,
                          fontWeight: FontWeight.bold
                      ),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),

            ],
          ),
        ),
        if(isLast) SizedBox(height: 20,)
      ],
    );
  }
}
