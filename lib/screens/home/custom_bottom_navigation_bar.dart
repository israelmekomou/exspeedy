import 'package:exspeedy/screens/home/BottomNavBarItem.dart';
import 'package:exspeedy/utils/constants.dart';
import 'package:exspeedy/utils/size_config.dart';
import 'package:flutter/material.dart';


class CustomBottomNavigationBar extends StatefulWidget {
  final int defaultSelectedIndex;
  final List<BottomNavBarItem> navBarItems;
  final Function(int) onIndexChange;

  CustomBottomNavigationBar({this.defaultSelectedIndex = 0, @required this.navBarItems, @required this.onIndexChange});

  @override
  _CustomBottomNavigationBarState createState() => _CustomBottomNavigationBarState();
}

class _CustomBottomNavigationBarState extends State<CustomBottomNavigationBar> {
  int _selectedItemIndex = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _selectedItemIndex = widget.defaultSelectedIndex;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
        children: _buildNavBarItems()
    );
  }

  List<Widget> _buildNavBarItems(){
    List<Widget> rowItems = [];
    for(int i = 0; i < widget.navBarItems.length; i++){
      BottomNavBarItem item = widget.navBarItems[i];

      rowItems.add(buildNavBarItem(
          icon: item.icon,
          title: item.title,
          index: i
      ));
    }
    return rowItems;
  }

  Widget buildNavBarItem({icon, index, title}){

    return GestureDetector(
      onTap: (){
        setState(() {
          _selectedItemIndex = index;
          widget.onIndexChange(index);
        });
      },
      child: Container(
        height: 60,
        width: MediaQuery.of(context).size.width/widget.navBarItems.length,
        decoration: index == _selectedItemIndex ? BoxDecoration(
          border: Border(
              bottom: BorderSide(
                  width: 4,
                  color: kPrimaryColor
              )
          ),
          gradient: LinearGradient(colors: [
            kPrimaryColor.withOpacity(0.3),
            kPrimaryColor.withOpacity(0.015),
          ],
            begin: Alignment.bottomCenter,
            end: Alignment.topCenter,
          ),
        ) : BoxDecoration(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              icon,
              color: index == _selectedItemIndex ? kPrimaryColor : Colors.grey,
            ),
            SizedBox(height: getScreenHeight(4),),
            Text(
              title,
              style: TextStyle(
                fontSize: getScreenWidth(10),
                color: index == _selectedItemIndex ? kPrimaryColor : kTextColor
              ),
            )
          ],
        ),
      ),
    );
  }
}