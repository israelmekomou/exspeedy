import 'package:flutter/material.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: 30,
            width: 30,
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
              strokeWidth: 2.5,
            ),

          ),
          SizedBox(
            height: 10,
          ),
          Text(
            'Chergement en cours',
            style: TextStyle(
              color: Colors.white
            ),
          )
        ],
      ),
    );
  }
}
