import 'package:exspeedy/models/address.dart';
import 'package:exspeedy/models/order.dart';
import 'package:exspeedy/models/order_extras.dart';
import 'package:exspeedy/models/order_info.dart';
import 'package:exspeedy/models/order_payment.dart';
import 'package:exspeedy/models/order_resume.dart';
import 'package:exspeedy/models/user.dart';
import 'package:exspeedy/providers/order_provider.dart';
import 'package:exspeedy/providers/user_provider.dart';
import 'package:exspeedy/screens/checkout/step_description.dart';
import 'package:exspeedy/screens/checkout/step_expedition.dart';
import 'package:exspeedy/screens/checkout/step_receiver.dart';
import 'package:exspeedy/utils/default_button.dart';
import 'package:exspeedy/utils/functions.dart';
import 'package:exspeedy/utils/size_config.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter/material.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  int _currentStep = 0;
  final _formKeyDescription = GlobalKey<FormState>();
  final _formKeyExpedition = GlobalKey<FormState>();
  final _formKeyReceiver = GlobalKey<FormState>();
  Order order = Order();
  OrderInfo info = OrderInfo();
  OrderResume resume = OrderResume();
  OrderPayment payment = OrderPayment();
  User sender = User(), receiver = User(), _user;

  @override
  void initState() {
    _user = context.read(userStateNotifierProvider.state);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Stepper(
        physics: NeverScrollableScrollPhysics(),
        currentStep: _currentStep,
        steps: _checkoutSteps(),
        onStepContinue: (){
          if(_currentStep < _checkoutSteps().length - 1){
            setState(() {
              _currentStep++;
            });
          }
        },
        onStepTapped: (step){
          if(_currentStep > step){
            setState(() {
              _currentStep = step;
            });
          }
        },
        controlsBuilder: (BuildContext context,
            {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
          return Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: SizedBox(
              width: double.infinity,
              height: getScreenHeight(60),
              child: DefaultButton(
                onPress: (){
                  switch(_currentStep){
                    case 0:
                      if(_formKeyDescription.currentState.validate()){
                        _formKeyDescription.currentState.save();
                        onStepContinue();
                      }
                      break;
                    case 1:
                      if(_formKeyExpedition.currentState.validate()){
                        _formKeyExpedition.currentState.save();
                        onStepContinue();
                      }
                      break;
                    case 2:
                      if(_formKeyReceiver.currentState.validate()){
                        _formKeyReceiver.currentState.save();
                        onStepContinue();
                      }
                      break;
                  }
                },
                text: _currentStep == _checkoutSteps().length - 1 ? 'Envoyer' : 'Continuer',
              ),
            ),
          );
        },
      ),
    );
  }

  List<Step> _checkoutSteps(){
    return [
      Step(
        title: Text('Description du colis'),
        isActive: _currentStep >= 0,
        content: StepDescription(_formKeyDescription, (description, size){
          resume.module = 'delivery';
          resume.description = "$description ($size)";
          resume.deliveryAmount = 1000;
        })
      ),
      Step(
        title: Text('Expédition'),
        isActive: _currentStep >= 1,
        content: StepExpedition(
          formKey: _formKeyExpedition,
          function: (c1, c2, q1, q2){
            info.city = c1;

            Address senderAddress = Address();
            senderAddress.ville = c1;
            senderAddress.quartier = q1;
            if(_user != null)sender = _user;
            sender.adresses = [senderAddress];

            Address receiverAddress = Address();
            receiverAddress.ville = c2;
            receiverAddress.quartier = q2;
            receiver.adresses = [receiverAddress];
          },
        ),
      ),
      Step(
        title: Text('Bénéficiaire'),
        isActive: _currentStep >= 2,
        content: StepReceiver(
          formKey: _formKeyReceiver,
          function: (name, phone){
            print('$name:$phone');
            receiver.fullname = name;
            receiver.telephone = phone;
            placeOrder();
          },
        ),
      )
    ];
  }

  placeOrder() async{
    hideKeyBoard();
    payment.message = "";
    payment.payMode = "cash";
    payment.paymentDate = getDateTime();

    info.origin = "ExSpeedy";
    info.platform = "Mobile";
    info.shippingDate = getDateTime(days: 1);
    info.pickingDate = getDateTime();

    order.user = _user;
    order.orderInfo = info;
    order.orderResume = resume;
    order.orderPayment = payment;
    order.sender = sender;
    order.receiver = receiver;
    order.orderExtras = OrderExtras();
    print(order.toString());
    final _orderRepository = context.read(orderRepositoryProvider);
    _orderRepository.placeOrder(order);
  }
}

