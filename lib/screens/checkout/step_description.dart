import 'package:exspeedy/utils/constants.dart';
import 'package:exspeedy/utils/functions.dart';
import 'package:exspeedy/utils/size_config.dart';
import 'package:flutter/material.dart';

class StepDescription extends StatefulWidget {
  final GlobalKey<FormState> formKey;
  final Function(String description, String size) loadInfo;


  StepDescription(this.formKey, this.loadInfo);

  @override
  _StepDescriptionState createState() => _StepDescriptionState();
}

class _StepDescriptionState extends State<StepDescription> {
  int _selectedSize = 0;
  String _selectedSizeString = "";
  bool setError = false;
  @override
  Widget build(BuildContext context) {
    return Form(
      key: widget.formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextFormField(
            focusNode: FocusNode(canRequestFocus: false),
            onSaved: (value){
              widget.loadInfo(value, _selectedSizeString);
            },
            validator: (value){
              if(value.isEmpty){
                return "";
              }else if(value.length < 6){
                return "Veuillez entrer une description plus longue";
              }
              return null;
            },
            maxLines: 4,
            decoration: InputDecoration(
              hintText: 'Veuillez décrire votre colis',
            ),
          ),
          SizedBox(height: getScreenHeight(30),),
          Text(
            'Veuillez choisir la taille de votre colis:',
            style: TextStyle(
              fontWeight: FontWeight.w600,
              color: setError ? Colors.red : kTextColor
            ),
          ),
          SizedBox(height: getScreenHeight(20),),
          FormField(
            validator: (v){
              setError = false;
              if(_selectedSize == 0){
                setState(() {
                  setError = true;
                });
                return "";
              }
              return null;
            },
            builder: (value){
              return Row(
                children: [
                  _sizeItem('Petit', 1),
                  Spacer(),
                  _sizeItem('Moyen', 2),
                  Spacer(),
                  _sizeItem('Large', 3),
                ],
              );
            },
          ),
          SizedBox(height: getScreenHeight(50),)
        ],
      ),
    );
  }

  _sizeItem(String text, int index){
    return Column(
      children: [
        GestureDetector(
          onTap: (){
            setState(() {
              _selectedSize = index;
              _selectedSizeString = text;
              hideKeyBoard();
            });
          },
          child: Container(
            height: 60,
            width: 60,
            decoration: _selectedSize == index ? BoxDecoration(
                gradient: LinearGradient(colors: [
                  kPrimaryColor.withOpacity(0.3),
                  kPrimaryColor.withOpacity(0.1),
                ],
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                ),
                shape: BoxShape.circle
            ) : BoxDecoration(
                shape: BoxShape.circle,
              color: Colors.grey.withOpacity(0.2)
            ),
            child: Center(
              child: Text(
                text[0].toUpperCase(),
                style: TextStyle(
                  fontSize: getScreenWidth(16)
                ),
              ),
            ),
          ),
        ),
        SizedBox(height: getScreenHeight(10),),
        Text(text)
      ],
    );
  }
}
