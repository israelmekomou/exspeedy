import 'package:exspeedy/screens/checkout/body.dart';
import 'package:flutter/material.dart';

class CheckoutScreen extends StatelessWidget {
  static String routeName = "/checkout";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Nouvelle Expédition'),
      ),
      body: Body(),
    );
  }
}