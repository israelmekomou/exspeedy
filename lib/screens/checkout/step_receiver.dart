import 'package:exspeedy/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class StepReceiver extends StatefulWidget {
  final GlobalKey<FormState> formKey;
  final Function(String name, String phone) function;


  StepReceiver({this.formKey, this.function});

  @override
  _StepReceiverState createState() => _StepReceiverState();
}

class _StepReceiverState extends State<StepReceiver> {
  String _name, _phone;
  @override
  Widget build(BuildContext context) {
    return Form(
      key: widget.formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextFormField(
            focusNode: FocusNode(canRequestFocus: false),
            onSaved: (value){
              widget.function(value, _phone);
            },
            validator: (value){
              if(value.isEmpty){
                return "";
              }
              return null;
            },
            decoration: InputDecoration(
              hintText: 'Nom et prénom',
            ),
          ),
          SizedBox(height: getScreenHeight(10),),
          TextFormField(
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))
            ],
            focusNode: FocusNode(canRequestFocus: false),
            onChanged: (text){
              _phone = text;
            },
            validator: (value){
              if(value.isEmpty){
                return "";
              }
              return null;
            },
            decoration: InputDecoration(
              hintText: 'Téléphone',
            ),
          ),
          SizedBox(height: getScreenHeight(50),)
        ],
      ),
    );
  }
}
