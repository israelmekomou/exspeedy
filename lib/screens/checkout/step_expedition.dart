import 'package:exspeedy/utils/constants.dart';
import 'package:exspeedy/utils/functions.dart';
import 'package:exspeedy/utils/size_config.dart';
import 'package:flutter/material.dart';

class StepExpedition extends StatefulWidget {
  final GlobalKey<FormState> formKey;
  final Function(String c1, String c2, String q1, String q2) function;


  StepExpedition({this.formKey, this.function});

  @override
  _StepExpeditionState createState() => _StepExpeditionState();
}

class _StepExpeditionState extends State<StepExpedition> {
  String _selectedCityStart, _selectedQuarterStart, _selectedCityEnd, _selectedQuarterEnd;
  @override
  Widget build(BuildContext context) {
    return Form(
      key: widget.formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Text(
                    'De: '
                ),
              ),
              Expanded(
                flex: 3,
                child: DropdownButtonFormField<String>(
                  hint: Text('Ville'),
                  focusNode: FocusNode(canRequestFocus: false),
                  autofocus: false,
                  onSaved: save(),
                  value: _selectedCityStart,
                  validator: (s){
                   if(s == null){
                     return '';
                   }
                   return null;
                  },
                  items: ['Douala', 'Yaoundé']
                      .map((e) => DropdownMenuItem(
                    child: Text(e),
                    value: e,
                  )).toList(),
                  onChanged: (s){
                    setState(() {
                      _selectedCityStart = s;
                      _selectedQuarterStart = null;
                      hideKeyBoard();
                    });
                  },
                ),
              ),
              SizedBox(width: getScreenWidth(10),),
              Expanded(
                flex: 4,
                child: DropdownButtonFormField<String>(
                  isExpanded: true,
                  hint: Text('Quartier'),
                  focusNode: FocusNode(canRequestFocus: false),
                  value: _selectedQuarterStart,
                  validator: (s){
                    if(s == null){
                      return '';
                    }
                    return null;
                  },
                  items: _selectedCityStart == 'Douala' ? Quarters.douala
                      .map((e) => DropdownMenuItem(
                    child: Text(e),
                    value: e,
                  )).toList() : Quarters.yaounde
                      .map((e) => DropdownMenuItem(
                    child: Text(e),
                    value: e,
                  )).toList() ,
                  onChanged: (s){
                    setState(() {
                      _selectedQuarterStart = s;
                    });
                  },
                ),
              ),
            ],
          ),
          SizedBox(height: getScreenHeight(20),),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Text(
                    'A: '
                ),
              ),
              Expanded(
                flex: 3,
                child: DropdownButtonFormField<String>(
                  hint: Text('Ville'),
                  focusNode: FocusNode(canRequestFocus: false),
                  autofocus: false,
                  value: _selectedCityEnd,
                  validator: (s){
                    if(s == null){
                      return '';
                    }
                    return null;
                  },
                  items: ['Douala', 'Yaoundé']
                      .map((e) => DropdownMenuItem(
                    child: Text(e),
                    value: e,
                  )).toList(),
                  onChanged: (s){
                    setState(() {
                      _selectedCityEnd = s;
                      _selectedQuarterEnd = null;
                      hideKeyBoard();
                    });
                  },
                ),
              ),
              SizedBox(width: getScreenWidth(10),),
              Expanded(
                flex: 4,
                child: DropdownButtonFormField<String>(
                  isExpanded: true,
                  hint: Text('Quartier'),
                  focusNode: FocusNode(canRequestFocus: false),
                  value: _selectedQuarterEnd,
                  validator: (s){
                    if(s == null){
                      return '';
                    }
                    return null;
                  },
                  items: _selectedCityEnd == 'Douala' ? Quarters.douala
                      .map((e) => DropdownMenuItem(
                    child: Text(e),
                    value: e,
                  )).toList() : Quarters.yaounde
                      .map((e) => DropdownMenuItem(
                    child: Text(e),
                    value: e,
                  )).toList() ,
                  onChanged: (s){
                    setState(() {
                      _selectedQuarterEnd = s;
                    });
                  },
                ),
              ),
            ],
          ),
          SizedBox(height: getScreenHeight(50),)
        ],
      ),
    );
  }

  save(){
    widget.function(_selectedCityStart, _selectedQuarterEnd
        , _selectedQuarterStart, _selectedQuarterEnd);
  }
}
