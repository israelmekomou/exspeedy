import 'package:exspeedy/providers/authentication_provider.dart';
import 'package:exspeedy/screens/sign_up/sign_up_screen.dart';
import 'package:exspeedy/utils/constants.dart';
import 'package:exspeedy/utils/dialog_helper.dart';
import 'package:exspeedy/utils/functions.dart';
import 'package:exspeedy/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  final _formKey = GlobalKey<FormState>();
  String login, password;
  DialogHelper _dialogHelper;
  TextEditingController _loginController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  @override
  void initState() {
    initProgressDialog();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: getScreenWidth(20)),
            child: Column(
              children: [
                SizedBox(
                  height: SizeConfig.screenHeight * 0.15,
                ),
                Image.asset(
                  "assets/images/logo.png",
                  height: getScreenHeight(70),
                ),
                SizedBox(
                  height: SizeConfig.screenHeight * 0.08,
                ),
                _buildSignInForm(),
                SizedBox(
                  height: SizeConfig.screenHeight * 0.1,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Vous êtes nouveau?'),
                    FlatButton(
                      onPressed: () {
                        Navigator.of(context).pushNamed(SignUpScreen.routeName);
                      },
                      child: Text(
                        'Créer un compte',
                        style: TextStyle(color: kPrimaryColor),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: SizeConfig.screenHeight * 0.1,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    FlatButton(
                      onPressed: () {},
                      child: Text(
                        'Passer >>',
                        style: TextStyle(color: Colors.grey[700]),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _buildSignInForm() {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          TextFormField(
            controller: _loginController,
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))
            ],
            validator: (v) {
              if (v.isEmpty) {
                return 'Entrez votre numéro de téléphone';
              }
              return null;
            },
            decoration: InputDecoration(
              hintText: 'Téléphone',
            ),
          ),
          SizedBox(
            height: getScreenHeight(15),
          ),
          TextFormField(
            obscureText: true,
            controller: _passwordController,
            validator: (v) {
              if (v.isEmpty) {
                return "Entrez votre mot de passe";
              }
              return null;
            },
            decoration: InputDecoration(hintText: 'Mot de passe'),
          ),
          SizedBox(
            height: getScreenHeight(25),
          ),
          RaisedButton(
            onPressed: () {
              hideKeyBoard();
              if (_formKey.currentState.validate()) {
                _dialogHelper.show();
                _login();
              }
              // Navigator.of(context).pushNamed(HomeScreen.routeName);
            },
            child: Container(
              width: double.infinity,
              height: getScreenHeight(60),
              child: Center(
                  child: Text(
                'Connexion',
                style: TextStyle(
                    fontSize: getScreenWidth(16), color: Colors.white),
              )),
            ),
          ),
        ],
      ),
    );
  }

  void initProgressDialog() {
    _dialogHelper = DialogHelper(context,
        isDismissible: false,
        body: Center(
          child: SizedBox(
            height: getScreenWidth(40),
            width: getScreenWidth(40),
            child: CircularProgressIndicator(),
          ),
        ));
  }

  Future<void> _login() async {
    final _authNotifier = context.read(authenticationNotifierProvider);
    await _authNotifier.loginWithPassword(
        _loginController.text, _passwordController.text);
    _dialogHelper.hide();
  }
}
