import 'package:exspeedy/models/order.dart';

abstract class OrderInterface{
  Future<bool> placeOrder(Order order);

  Future<List<Order>> getOrders();
}