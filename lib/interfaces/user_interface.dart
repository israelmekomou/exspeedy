import 'package:exspeedy/models/user.dart';

abstract class UserInterface{
  Future<User> getUserProfile();

  Future<User> updateUserProfile();
}