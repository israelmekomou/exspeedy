import 'package:exspeedy/utils/constants.dart';
import 'package:exspeedy/utils/size_config.dart';
import 'package:flutter/material.dart';


class DefaultBackground extends StatelessWidget {
  final Widget title, body;
  final int position;

  DefaultBackground({@required this.title, @required this.body, this.position = -1});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kPrimaryColor,
      child: Column(
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                color: kPrimaryColor,
              ),
              child: Column(
                children: [
                  Container(
                    width: double.infinity,
                    height: SizeConfig.screenHeight * 0.1,
                    child: Center(
                      child: title,
                    ),
                  ),
                  Expanded(
                    child: Container(
                      decoration: position == 1 || position == -1 ? BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30.0),
                            topRight: Radius.circular(30.0),
                          )
                      ) : BoxDecoration(
                          color: Colors.white
                      ),
                      child: body,
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
