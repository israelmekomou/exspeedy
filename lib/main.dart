import 'package:exspeedy/models/address.dart';
import 'package:exspeedy/models/order.dart';
import 'package:exspeedy/models/order_extras.dart';
import 'package:exspeedy/models/order_info.dart';
import 'package:exspeedy/models/order_payment.dart';
import 'package:exspeedy/models/order_resume.dart';
import 'package:exspeedy/models/user.dart';
import 'package:exspeedy/utils/authentication_manager.dart';
import 'package:exspeedy/utils/constants.dart';
import 'package:exspeedy/utils/routes.dart';
import 'package:exspeedy/utils/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:object_mapper/object_mapper.dart';

void main() {
  Mappable.factories = {
    User : () => User(),
    Address : () => Address(),
    Order: () => Order(),
    OrderExtras: () => OrderExtras(),
    OrderInfo: () => OrderInfo(),
    OrderPayment: () => OrderPayment(),
    OrderResume: () => OrderResume(),
  };
  WidgetsFlutterBinding.ensureInitialized();
  runApp(ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: kAppName,
      theme: theme(),
      initialRoute: AuthenticationManager.routeName,
      routes: routes,
    );
  }
}

