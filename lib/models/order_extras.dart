import 'package:object_mapper/object_mapper.dart';

class OrderExtras with Mappable{
  String downloadBill;
  String billLink;
  String cartOrder;
  String products;
  String downloadCartBill;
  String cartBillLink;

  @override
  void mapping(Mapper map) {
    map('facture_download', downloadBill, (v) => downloadBill = v);
    map('facture_link', billLink, (v) => billLink = v);
    map('commandes_achats', cartOrder, (v) => cartOrder = v);
    map('produits', products, (v) => products = v);
    map('facture_achats_download', downloadCartBill, (v) => downloadCartBill = v);
    map('facture_achats_link', cartBillLink, (v) => cartBillLink = v);
  }

  @override
  String toString() {
    return 'OrderExtras{downloadBill: $downloadBill, billLink: $billLink, cartOrder: $cartOrder, products: $products, downloadCartBill: $downloadCartBill, cartBillLink: $cartBillLink}';
  }
}