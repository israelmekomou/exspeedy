import 'package:object_mapper/object_mapper.dart';

class OrderPayment with Mappable {
  String message;
  String payMode;
  String status;
  String paymentDate;
  int totalAmount;
  int paymentId;
  String paymentIntent;

  @override
  void mapping(Mapper map) {
    map('message', message, (v) => message = v);
    map('mode_paiement', payMode, (v) => payMode = v);
    map('statut', status, (v) => status = v);
    map('date_paiement', paymentDate, (v) => paymentDate = v);
    map('montant_total', totalAmount, (v) => totalAmount = v);
    map('id_paiement', paymentId, (v) => paymentId = v);
    map('payment_intent', paymentIntent, (v) => paymentIntent = v);
  }

  Map<String, dynamic> toMap(){
    return {
      'message' : message,
      'mode_paiement' : payMode,
      'statut' : status,
      'date_paiement' : paymentDate,
      'montant_total' : totalAmount,
      'id_paiement' : paymentId,
      'payment_intent' : paymentIntent,
    };
  }

  @override
  String toString() {
    return 'OrderPayment{message: $message, payMode: $payMode, status: $status, paymentDate: $paymentDate, totalAmount: $totalAmount, paymentId: $paymentId, paymentIntent: $paymentIntent}';
  }
}
