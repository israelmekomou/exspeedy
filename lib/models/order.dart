import 'package:exspeedy/models/order_extras.dart';
import 'package:exspeedy/models/order_info.dart';
import 'package:exspeedy/models/order_payment.dart';
import 'package:exspeedy/models/order_resume.dart';
import 'package:exspeedy/models/user.dart';
import 'package:object_mapper/object_mapper.dart';

class Order with Mappable{
  OrderInfo orderInfo;
  OrderResume orderResume;
  OrderPayment orderPayment;
  OrderExtras orderExtras;
  User user;
  User sender;
  User receiver;
  @override
  void mapping(Mapper map) {
    map<OrderInfo>('infos', orderInfo, (v) => orderInfo = v);
    map<OrderResume>('orders', orderResume, (v) => orderResume = v);
    map<OrderPayment>('paiement', orderPayment, (v) => orderPayment = v);
    map<OrderExtras>('extra', orderExtras, (v) => orderExtras = v);
    map<User>('client', user, (v) => user = v);
    map<User>('sender', sender, (v) => sender = v);
    map<User>('receiver', receiver, (v) => receiver = v);
  }

  Map<String, dynamic> toMap(){
    return{
      'infos' : orderInfo.toMap(),
      'orders' : orderResume.toMap(),
      'paiement' : orderPayment.toMap(),
      'client' : user,
      'sender' : sender,
      'receiver' : receiver,
    };
  }

  @override
  String toString() {
    return 'Order{orderInfo: $orderInfo, orderResume: $orderResume, orderPayment: $orderPayment, orderExtras: $orderExtras, client: $user, sender: $sender, receiver: $receiver}';
  }
}