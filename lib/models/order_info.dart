import 'package:object_mapper/object_mapper.dart';

class OrderInfo with Mappable{
  int id;
  String ref;
  String origin;
  String platform;
  String pickingDate;
  String shippingDate;
  String shippingTime;
  String shippingDay;
  String city;
  int status;
  String humanStatus;
  String type;
  int distance;
  String distanceText;
  int duration;
  String durationText;
  String content;
  String creationDate;

  @override
  void mapping(Mapper map) {
    map('id', id, (v) => id = v);
    map('ref', ref, (v) => ref = v);
    map('origin', origin, (v) => origin = v);
    map('platform', platform, (v) => platform = v);
    map('date_chargement', pickingDate, (v) => pickingDate = v);
    map('date_livraison', shippingDate, (v) => shippingDate = v);
    map('heure_livraison', shippingTime, (v) => shippingTime = v);
    map('jour_livraison', shippingDay, (v) => shippingDay = v);
    map('ville_livraison', city, (v) => city = v);
    map('statut', status, (v) => status = v);
    map('statut_human', humanStatus, (v) => humanStatus = v);
    map('type', type, (v) => type = v);
    map('distance', distance, (v) => distance = v);
    map('distance_text', distanceText, (v) => distanceText = v);
    map('duration', duration, (v) => duration = v);
    map('duration_text', durationText, (v) => durationText = v);
    map('contenu', content, (v) => content = v);
    map('date_creation', creationDate, (v) => creationDate = v);
  }

  Map<String, dynamic> toMap(){
    return {
      'origin' : origin,
      'platform' : platform,
      'date_chargement' : pickingDate,
      'date_livraison' : shippingDate,
      'heure_livraison' : shippingTime,
      'jour_livraison' : shippingDay,
      'ville_livraison' : city,
      'contenu' : content
    };
  }

  @override
  String toString() {
    return 'OrderInfo{id: $id, ref: $ref, origin: $origin, platform: $platform, pickingDate: $pickingDate, shippingDate: $shippingDate, shippingTime: $shippingTime, shippingDay: $shippingDay, city: $city, status: $status, humanStatus: $humanStatus, type: $type, distance: $distance, distanceText: $distanceText, duration: $duration, durationText: $durationText, content: $content, creationDate: $creationDate}';
  }
}