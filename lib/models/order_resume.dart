import 'package:object_mapper/object_mapper.dart';

class OrderResume with Mappable{
  String module;
  String description;
  int deliveryAmount;
  int totalAmount;
  int shopId;
  String codePromo;
  String comment;

  @override
  void mapping(Mapper map) {
    map('module', module, (v) => module = v);
    map('description', description, (v) => description = v);
    map('montant_livraison', deliveryAmount, (v) => deliveryAmount = v);
    map('montant_total', totalAmount, (v) => totalAmount = v);
    map('magasin_id', shopId, (v) => shopId = v);
    map('code_promo', codePromo, (v) => codePromo = v);
    map('commentaire', comment, (v) => comment = v);
  }

  Map<String, dynamic> toMap() {
    return {
      'module' : module,
      'description' : description,
      'montant_livraison' : deliveryAmount,
      'montant_total' : totalAmount,
      'magasin_id' : shopId,
      'code_promo' : codePromo,
      'commentaire' : comment,
    };
  }

  @override
  String toString() {
    return 'OrderResume{module: $module, description: $description, deliveryAmount: $deliveryAmount, totalAmount: $totalAmount, shopId: $shopId, codePromo: $codePromo, comment: $comment}';
  }
}