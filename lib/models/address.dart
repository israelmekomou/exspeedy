import 'package:object_mapper/object_mapper.dart';

class Address with Mappable{
  int id;
  int ville_id;
  String titre;
  String surnom;
  String quartier;
  String description;
  String nom;
  String adresse;
  String latitude;
  String longitude;
  String latitude_longitude;
  String ville;
  String pays;
  int provider_id;
  String provider_name;
  String date_creation;
  String date_modification;
  bool est_favorite;

  @override
  void mapping(Mapper map) {
    map('id', id, (v) => id = v);
    map("ville_id", ville_id, (v) => ville_id = v);
    map("titre", titre, (v) => titre = v);
    map("surnom", surnom, (v) => surnom = v);
    map("quartier", quartier, (v) => quartier = v);
    map("description", description, (v) => description = v);
    map("nom", nom, (v) => nom = v);
    map("adresse", adresse, (v) => adresse = v);
    map("latitude", latitude, (v) => latitude = v);
    map("longitude", longitude, (v) => longitude = v);
    map("latitude_longitude", latitude_longitude, (v) => latitude_longitude = v);
    map("ville", ville, (v) => ville = v);
    map("pays", pays, (v) => pays = v);
    map("provider_id", provider_id, (v) => provider_id = v);
    map("provider_name", provider_name, (v) => provider_name = v);
    map("date_creation", date_creation, (v) => date_creation = v);
    map("date_modification", date_modification, (v) => date_modification = v);
    map("est_favorite", est_favorite, (v) => est_favorite = v);
  }

  @override
  String toString() {
    return 'Address{id: $id, ville_id: $ville_id, titre: $titre, surnom: $surnom, quartier: $quartier, description: $description, nom: $nom, adresse: $adresse, latitude: $latitude, longitude: $longitude, latitude_longitude: $latitude_longitude, ville: $ville, pays: $pays, provider_id: $provider_id, provider_name: $provider_name, date_creation: $date_creation, date_modification: $date_modification, est_favorite: $est_favorite}';
  }
}

