import 'package:exspeedy/models/address.dart';
import 'package:object_mapper/object_mapper.dart';

class User with Mappable{
  String id;
  List<Address> adresses;
  String uid;
  String fullname;
  String firstname;
  String lastname;
  String email = "";
  String telephone = "";
  String telephone_alt;
  String note_interne;
  String provider_id;
  String provider_name;
  String phone_verified_at;
  String email_verified_at;
  String updated_at;
  String token;

  @override
  void mapping(Mapper map) {
    map("id", id, (v) => id = v.toString());
    map("fullname", fullname, (v) => fullname = v);
    map("firstname", firstname, (v) => firstname = v);
    map("lastname", lastname, (v) => lastname = v);
    map("email", email, (v) => email = v);
    map("telephone", telephone, (v) => telephone = v);
    map("telephone_alt", telephone_alt, (v) => telephone_alt = v);
    map("note_interne", note_interne, (v) => note_interne = v);
    map("provider_id", provider_id, (v) => provider_id = v);
    map("provider_name", provider_name, (v) => provider_name = v);
    map("phone_verified_at", phone_verified_at, (v) => phone_verified_at = v);
    map("email_verified_at", email_verified_at, (v) => email_verified_at = v);
    map("updated_at", updated_at, (v) => updated_at = v);
    map<Address>("adresses", adresses, (v) => adresses = v);
  }

  @override
  String toString() {
    return 'User{id: $id, adresses: $adresses, uid: $uid, fullname: $fullname, firstname: $firstname, lastname: $lastname, email: $email, telephone: $telephone, telephone_alt: $telephone_alt, note_interne: $note_interne, provider_id: $provider_id, provider_name: $provider_name, phone_verified_at: $phone_verified_at, email_verified_at: $email_verified_at, updated_at: $updated_at, token: $token}';
  }
}