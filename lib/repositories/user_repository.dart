import 'package:dio/dio.dart';
import 'package:exspeedy/interfaces/user_interface.dart';
import 'package:exspeedy/models/user.dart';
import 'package:exspeedy/repositories/remote_repository.dart';
import 'package:exspeedy/utils/constants.dart';
import 'package:object_mapper/object_mapper.dart';

class UserRepository extends RemoteRepository implements UserInterface{
  UserRepository(Dio client) : super(client);

  @override
  Future<User> getUserProfile() async{
    final _userRoute = ApiRoutes.getUser;
    try{
      final result = await client.get(_userRoute);
      print(result);
      final user = Mapper.fromJson(result.data['data']).toObject<User>();
      return user;
    }catch (err){
      print(err);
      return null;
    }
  }

  @override
  Future<User> updateUserProfile() {
    // TODO: implement updateUserProfile
    throw UnimplementedError();
  }

}