import 'package:dio/dio.dart';
import 'package:exspeedy/interfaces/order_interface.dart';
import 'package:exspeedy/models/order.dart';
import 'package:exspeedy/repositories/remote_repository.dart';
import 'package:exspeedy/utils/constants.dart';
import 'package:object_mapper/object_mapper.dart';

class OrderRepository extends RemoteRepository implements OrderInterface{
  OrderRepository(Dio client) : super(client);

  @override
  Future<List<Order>> getOrders() async{
    final _orderRoute = ApiRoutes.order;
    print('getOrder');
    try{
      final result = await client.get<Map<String, dynamic>>(_orderRoute);
      final _orders = (result.data['data'] as List).map((e) => Mapper.fromJson(e).toObject<Order>()).toList();
      return _orders;
    }catch(err){
      if(err is DioError){
      }
      return <Order>[];
    }
  }

  @override
  Future<bool> placeOrder(Order order) async{
    final _orderRoute = ApiRoutes.order;
    try{
      print('placeOrder');
      final result = await client.post(_orderRoute, data: order.toMap());
      print(result);
      return true;
    }catch(err){
      if(err is DioError){
        print(err.response);
      }
      return false;
    }
  }

}