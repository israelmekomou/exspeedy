// import 'dart:convert';
//
// import 'package:exspeedy/models/user.dart';
// import 'package:exspeedy/screens/home/home_screen.dart';
// import 'package:exspeedy/utils/constants.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:http/http.dart';
// import 'package:object_mapper/object_mapper.dart';
// import 'package:shared_preferences/shared_preferences.dart';
//
// class AuthenticationService {
//   final BuildContext context;
//
//
//   AuthenticationService(this.context);
//
//   Future signInWithPhone(String phone, String password) async {
//     try {
//       final headers = {
//         'Content-Type': 'application/json',
//         'Accept': 'application/json'
//       };
//       Map<String, dynamic> body = {
//         'telephone': phone,
//         'password': password,
//         'phone_country_code': '237'
//       };
//       print(body.toString());
//       final url = Uri.https(kBaseUrl, '/api/v1.1/login/firebase/password');
//       final response = await post(
//           url, headers: headers, body: jsonEncode(body));
//       print(response.body.toString());
//       if (response.statusCode == 200) {
//         Map<String, dynamic> map = jsonDecode(response.body);
//         String token = "Bearer ${map['access_token']}";
//         getUserProfile(token);
//       } else {
//
//       }
//     } catch (e) {
//       print(e.toString());
//     }
//   }
//
//   Future createUser(firstName, lastName, phone, password) async {
//     final headers = {
//       'Accept': 'application/json',
//     };
//     final body = {
//       'Origin': 'exspeedy',
//       'firstname': firstName,
//       'lastname': lastName,
//       'email': "$firstName@$lastName.com",
//       'username': phone,
//       'password': password,
//       'password_confirmation': password,
//       'telephone': "+" + "237" + phone,
//       'phone_country_code': "237",
//       'telephone_alt': phone,
//       'licence': "true"
//     };
//     final url = Uri.https(kBaseUrl, '/api/v1.1/register/firebase');
//     final response = await post(url, headers: headers, body: body);
//     print(response.body.toString());
//   }
//
//   getUserProfile(String token) async{
//     SharedPreferences preferences = await SharedPreferences.getInstance();
//     preferences.setString('token', token);
//     final headers = {
//       'Accept': 'application/json',
//       'Authorization': token
//     };
//     final url = Uri.https(kBaseUrl, '/api/v1.1/user');
//     final response = await get(url, headers: headers);
//     if (response.statusCode == 200) {
//       Map<String, dynamic> map = jsonDecode(response.body)['data'];
//       User.currentUser = Mapper.fromJson(map).toObject<User>();
//       User.currentUser.token = token;
//       print(User.currentUser.toString());
//       Navigator.of(context).popAndPushNamed(HomeScreen.routeName);
//     }
//   }
// }