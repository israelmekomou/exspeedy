import 'package:exspeedy/screens/checkout/checkout_screen.dart';
import 'package:exspeedy/screens/home/home_screen.dart';
import 'package:exspeedy/screens/sign_in/sign_in_screen.dart';
import 'package:exspeedy/screens/sign_up/sign_up_screen.dart';
import 'package:exspeedy/screens/splash/splash_screen.dart';
import 'package:exspeedy/utils/authentication_manager.dart';
import 'package:flutter/material.dart';

final Map<String, WidgetBuilder> routes = {
  SplashScreen.routeName : (context) => SplashScreen(),
  AuthenticationManager.routeName : (context) => AuthenticationManager(),
  SignInScreen.routeName : (context) => SignInScreen(),
  HomeScreen.routeName : (context) => HomeScreen(),
  CheckoutScreen.routeName : (context) => CheckoutScreen(),
  SignUpScreen.routeName : (context) => SignUpScreen(),
};