import 'package:exspeedy/utils/constants.dart';
import 'package:exspeedy/utils/size_config.dart';
import 'package:flutter/material.dart';
class DefaultButton extends StatelessWidget {
  final String text;
  final Function onPress;

  DefaultButton({@required this.text, @required this.onPress});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: FlatButton(
        onPressed: onPress,
        color: kPrimaryColor,
        child: Container(
          width: SizeConfig.screenWidth * 0.6,
          height: getScreenHeight(60),
          child: Center(
            child: Text(
              text,
              style: TextStyle(
                  fontSize: getScreenWidth(16),
                  color: Colors.white
              ),
            ),
          ),
        ),
      ),
    );
  }
}
