import 'package:exspeedy/utils/functions.dart';
import 'package:flutter/material.dart';
bool _isShowing = false, _isDismissible = true;
BuildContext _context, _dismissingContext;
Widget _body;
class DialogHelper{

  DialogHelper(BuildContext context, {bool isDismissible = true, @required Widget body}){
    _context = context;
    _body = body;
    _isDismissible = isDismissible;
  }

  isShowing(){
    return _isShowing;
  }

  Future<bool> show() async{
    hideKeyBoard();
    try{
      if(!_isShowing){
        showDialog(
          context: _context,
          barrierDismissible: _isDismissible,
          builder: (BuildContext context){
            _dismissingContext = context;
            return WillPopScope(
              onWillPop: () async => _isDismissible,
              child: _body
            );
          }
        );
        _isShowing = true;
        return true;
      }else{
        return false;
      }
    }catch(e){
      _isShowing = false;
      return false;
    }
  }

  Future<bool> hide() async{
    try{
      if(_isShowing){
        _isShowing = false;
        Navigator.of(_dismissingContext).pop();
        return Future.value(true);
      }else{
        return Future.value(false);
      }
    }catch(e){
      return Future.value(false);
    }
  }

  static handle(context, dismissible, dialog) => showDialog(
      context: context,
      barrierDismissible: dismissible,
      builder: (context) => dialog
  );
}