import 'package:flutter/material.dart';
import 'package:intl/intl.dart';


hideKeyBoard() {
  FocusManager.instance.primaryFocus.unfocus();
}

String getDateTime({int days = 0}){
  var now = DateTime.now().add(Duration(days: days));
  var formatter = DateFormat('yyyy-MM-dd');
  String date = formatter.format(now);
  String hour = now.hour.toString().length == 1 ? "0" + now.hour.toString()
      : now.hour.toString();
  String minute = now.minute.toString().length == 1 ? "0" + now.minute.toString()
      : now.minute.toString();
  return "$date $hour:$minute:00";
}