import 'package:exspeedy/providers/authentication_notifier.dart';
import 'package:exspeedy/providers/authentication_provider.dart';
import 'package:exspeedy/screens/home/home_screen.dart';
import 'package:exspeedy/screens/sign_in/sign_in_screen.dart';
import 'package:exspeedy/screens/splash/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class AuthenticationManager extends StatefulWidget {
  static String routeName = 'manager';
  @override
  _AuthenticationManagerState createState() => _AuthenticationManagerState();
}

class _AuthenticationManagerState extends State<AuthenticationManager> {

  @override
  void initState() {
    final _authNotifier = context.read(authenticationNotifierProvider);
    _authNotifier.loginWithToken();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, watch, child){
        final _authState = watch(authenticationNotifierProvider.state);
        if(_authState is BaseAuthenticationState){
          return SplashScreen();
        } else if (_authState is UserConnectedState) {
          return HomeScreen();
        } else {
          return SignInScreen();
        }
      },
    );
  }
}
