import 'package:flutter/material.dart';

import 'constants.dart';

ThemeData theme(){
  final ThemeData base = ThemeData.light();
  return base.copyWith(
    primaryColor: kPrimaryColor,
    buttonTheme: base.buttonTheme.copyWith(
      buttonColor: kPrimaryColor
    ),
    accentColor: kPrimaryColor,
    scaffoldBackgroundColor: Colors.white,
    appBarTheme: appBarTheme(),
    textTheme: _textTheme(base.textTheme),
    primaryTextTheme: _textTheme(base.primaryTextTheme),
    accentTextTheme: _textTheme(base.accentTextTheme),
    inputDecorationTheme: InputDecorationTheme(
      border: OutlineInputBorder(
        borderSide: BorderSide(
          width: 2.0,
          color: kTextColor,
        ),
      ),
      focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
              width: 2.0,
              color: kPrimaryColor
          )
      ),
    )
  );
}

TextTheme _textTheme(TextTheme base){
  return  base.copyWith(
    headline5: base.headline5.copyWith(
        fontWeight: FontWeight.w500
    ),
    headline6: base.headline6.copyWith(
        fontSize: 18.0
    ),
    caption: base.caption.copyWith(
        fontWeight: FontWeight.w400,
        fontSize: 14.0
    ),
    bodyText1: base.bodyText1.copyWith(
        fontWeight: FontWeight.w500,
        fontSize: 16.0
    ),
  ).apply(
      fontFamily: 'Rubik',
      decorationColor: kTextColor,
      bodyColor: kTextColor
  );
}

AppBarTheme appBarTheme(){
  return AppBarTheme(
    color: kPrimaryColor,
    elevation: 0,
    brightness: Brightness.dark,
    iconTheme: IconThemeData(color: Colors.white),
    textTheme: TextTheme(
        headline6: TextStyle(color: Colors.white, fontSize: 18)
    ),
  );
}