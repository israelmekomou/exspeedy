import 'package:exspeedy/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

const kPrimaryColor = Color(0xFFF28C00);
const kPrimaryLightColor = Color(0xFFFFECDF);

const kSecondaryColor = Color(0xFF979797);
const kTextColor = Color(0xFF757575);

const kOverlay10 = Color(0x1A000000);
const kOverlay20 = Color(0x33000000);
const kOverlay30 = Color(0x4D000000);
const kOverlay40 = Color(0x66000000);

const kGrey5 = Color(0xFFf2f2f2);
// const kSecondaryColor = Color(0xFF37474F);
// const kTextColor = Color(0xFF263238);
// const KAnimationDuration = Duration(microseconds: 200);

const KAnimationDuration = Duration(microseconds: 300);

final headingStyle = TextStyle(
    fontSize: getScreenWidth(22),
    fontWeight: FontWeight.w600,
    color: Colors.white,
);


final RegExp emailValidatorRegExp = RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9.]+\.[a-zA-Z]+");
const kBaseUrl = "https://api.staging.livraison-express.net/api/v1.1/";
const kOrigin = "https://livraison-express.net";
const kAppName = 'SpeedEx';
const kSecureStorage = FlutterSecureStorage();

class ApiRoutes{
    static const login = 'login/firebase/password';
    static const register = 'register/firebase';
    static const getUser = 'user';
    static const order = 'user/delivery';
}

class SecureStorageKeys{
    static const authToken = 'authToken';
}

class Quarters{
    static const List<String> yaounde = [
        'ABANG',
        'ABATTOIR',
        'AKONO',
        'AHALA2',
        'AHALA BARRIERE',
        'ANGUISSA',
        'AYENE',
        'AWAE',
        'BIYEM-ASSI',
        'BITENG',
        'BITOL',
        'BASTOS',
        'CAMWATER',
        'CARREFOUR ETOA MEKI',
        'CENTRAL MEFOU',
        'CMC',
        'DIBANG',
        'DZENG',
        'EFOULAN',
        'EKOUNOU',
        'ELIG ESSONO',
        'ELIG EDZOA',
        'ESSOS',
        'FIBOT',
        'FOUDA',
        'FOUGEROL',
        'INGENIEURIE FORESTIERE',
        'INSTITUT SAMBA',
        'KIKOT',
        'KOMEKUI',
        'KONDENGUI',
        'LADA',
        'LOLODORF',
        'MAKAK',
        'MANGUIER',
        'MBANDJOCK',
        'MEFOUN',
        'MELEN',
        'MIMBOMAN',
        'MESSASSI',
        'MVAN',
        'MVOGADA',
        'NGOA EKELE',
        'NGOUMOU',
        'NGOUSSO',
        'NKOLBISSON',
        'NKOLEWOUE',
        'NKOMO',
        'NLONGKAK',
        'NSAM',
        'OBALA',
        'OLEMBE',
        'OMNISPORT',
        'ODZA',
        'OYOMABAN',
        "SA'A",
        'SOA',
        'SIMBOCK',
        'TROPICANA',
        'TSINGA'
    ];
    static const List<String> douala = [
        'AKWA',
        'AKWA BONAMOUTI',
        'AKWA EMILIE SAKER',
        'AKWA NORD',
        'AKWA NORD PADRE PIO',
        'AKWA NORD TERMINUS',
        'AKWA SOCAR',
        'AXE-LOURD DLA-YDE',
        'BALI',
        'BALI ENTRELEC',
        'BALI RUE DE LA JOIE',
        'BANGUE',
        'BASE ELF',
        'BASE NAVALE',
        'BASSA',
        'BASSA Z.I',
        'BASSA-CCC',
        'BEEDI',
        'BEPANDA',
        'BEPANDA AXE-LOURD',
        'BEPANDA TONNERRE',
        'BESSENGUE',
        'BEPENDA-OMNISPORT',
        'BONABASSEM',
        'BONABERI',
        'BONABERI BEKOKO',
        'BONABERI BOJONGO',
        'BONABERI BONASSAMA',
        'BONABERI MABANDA',
        'BONABERI SODIKO',
        'BONADIBONG',
        'BONAMOUSSADI',
        'BONAMOUSSADI DENVER',
        'BONAMOUSSADI SANTA BARBARA',
        'BONANJO',
        'BONAPRISO',
        "BONAPRISO ARMEE DE L'AIR",
        'BONAPRISO BONADOUMAHOME',
        'BONAPRISO CHOCOCHO',
        'BONAPRISO CIMETIERE',
        'BONAPRISO HYDROCARBURES',
        'BONAPRISO MARCHE DES FLEURS',
        'BONAPRISO RUE KOLOKO',
        'BONAPRISO RUE NJO NJO',
        'BONAPRISO USINE BRASSERIES',
        'BONATEKI',
        'BONATONE',
        'BORNE 10',
        'BRAZZAVILLE',
        'CAMP YABASSI',
        'CITE-CICAM',
        'CITE DES PALMIERS',
        'CITE-SIC',
        'DAKAR',
        'DEIDO',
        'DEIDO COAF',
        'DEIDO ECOLE PUBLIQUE',
        'DEIDO FEU ROUGE',
        'DEIDO GRAND-MOULIN',
        'ESSEC',
        'JAPOMA',
        'KM5-TERMINUS',
        'KOTTO',
        'KOTTO BLOC',
        'KOTTO IMMEUBLE',
        'KOTTO VILLAGE',
        'LENDI',
        'LOGBABA',
        'LOGBESSOU',
        'LOGPOM',
        'MADAGASCAR',
        'MAKEPE',
        'MAKEPE BM',
        'MAKEPE COURS SUPREME',
        'MAKEPE LYCEE',
        'MAKEPE PETIT PAYS',
        'MAKEPE ST TROPEZ',
        'MAKEPE MISSOKE',
        'MARCHE CONGO',
        'MBANYA',
        'SABLE',
        'MBOPPI',
        'NDOGBONG',
        'NDOGPASSI 1',
        'NDOGPASSI 2',
        'NDOGPASSI 3',
        'NDOGSIMBI',
        'NDOKOTI',
        'NEW-BELL',
        'NEW BELL CIMETIERE',
        'NEW BELL KASSALAFAM',
        'NEW BELL LYCEE',
        'NEW BELL MARCHE CENTRAL',
        'NEW BELL NKOLOULON',
        'NEW DEIDO',
        'NKONGMONDO',
        'NKOULOULOUN',
        'NYALLA',
        'PK8',
        'PK9',
        'PK10',
        'PK11',
        'PK12',
        'PK13',
        'PK14',
        'PK15',
        'PK16',
        'PK17',
        'PK18',
        'PK19',
        'PK20',
        'PK21',
        'PK22',
        'PK23',
        'PK24',
        'PK25',
        'ROND-POINT DEIDO',
        'VILLAGE',
        'YASSA',
        'YOUPWE'
    ];
}

// const kBaseUrl = "https://api.livraison-express.net/api/v1.1";
