import 'package:exspeedy/interfaces/authentication_interface.dart';
import 'package:exspeedy/providers/authentication_notifier.dart';
import 'package:exspeedy/providers/client_provider.dart';
import 'package:exspeedy/repositories/authentication_repository.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final authenticationRepositoryProvider = Provider<AuthenticationInterface>((ref){
  return AuthenticationRepository(ref.read(clientProvider.state));
});

final authenticationNotifierProvider = StateNotifierProvider<AuthenticationStateNotifier>((ref){
  final _authInterface = ref.read(authenticationRepositoryProvider);
  final _clientStateNotifier = ref.read(clientProvider);
  return AuthenticationStateNotifier(_authInterface, _clientStateNotifier);
});