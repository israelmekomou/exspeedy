import 'package:exspeedy/interfaces/user_interface.dart';
import 'package:exspeedy/models/user.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class UserStateNotifier extends StateNotifier<User>{
  final UserInterface _userInterface;

  UserStateNotifier(this._userInterface) : super(null);

  Future<void> getUserProfile() async{
    state = await _userInterface.getUserProfile();
    print(state);
    state;
  }

}