import 'package:exspeedy/interfaces/user_interface.dart';
import 'package:exspeedy/providers/user_state_notifier.dart';
import 'package:exspeedy/repositories/user_repository.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'client_provider.dart';

final userRepositoryProvider = Provider<UserInterface>((ref){
  final client = ref.watch(clientProvider.state);
  return UserRepository(client);
});

final userStateNotifierProvider = StateNotifierProvider<UserStateNotifier>((ref){
  final userRepository = ref.watch(userRepositoryProvider);
  return UserStateNotifier(userRepository);
});