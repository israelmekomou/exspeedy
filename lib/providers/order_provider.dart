import 'package:exspeedy/interfaces/order_interface.dart';
import 'package:exspeedy/providers/client_provider.dart';
import 'package:exspeedy/repositories/order_repository.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final orderRepositoryProvider = Provider<OrderInterface>((ref){
  final client = ref.watch(clientProvider.state);
  return OrderRepository(client);
});